console.log("Hello World");

// JAVASCRIPT Synchronous and Asynchronous

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json))

 async function fetchData () {
 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
 	console.log(result)
 	console.log(typeof result);

 	console.log(result.body)
 	let json = await result.json();
 	console.log(json)
 };

 fetchData();


 fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json))

// CREATING A FETCH


 fetch('https://jsonplaceholder.typicode.com/posts/', {
 		method: 'POST',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			title: 'New post',
 			body: 'Hello World',
 			userId: 1
 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))



// UPDATING POST

 fetch('https://jsonplaceholder.typicode.com/posts/1', {
 		method: 'PUT',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			title: 'Updated Post',
 			body: 'Hello again',
 			userId: 1
 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))


// // PATCH Method

 fetch('https://jsonplaceholder.typicode.com/posts/1', {
 		method: 'PATCH',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			title: 'Corrected post',

 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))



// DELETE

 fetch('https://jsonplaceholder.typicode.com/posts/1'), {
 		method: 'DELETE',


 })
.then((response) => response.json())
.then((json) => console.log(json))


// FILTERING THE POST

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
.then((response) => response.json())
.then((json) => console.log(json))


fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json))