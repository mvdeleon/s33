

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/')
.then((response) => response.json())
.then((json) => {

		let list = json.map((todo => {
			return todo.title;
		}))

console.log(list);
})

// SPECIFIC

fetch('https://jsonplaceholder.typicode.com/todos/')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// POST METHOD
 fetch('https://jsonplaceholder.typicode.com/todos/', {
 		method: 'POST',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			title: 'Created To Do List Item',
 			completed: false,
 			
 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))

// PUT METHOD

 fetch('https://jsonplaceholder.typicode.com/todos/1', {
 		method: 'PUT',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			title: 'Updated To Do List',
 			description: "To update my to do list with a different data structure",
 			status: "Pending",
 			dateCompleted: "Pending",
 			userID: 1
 			
 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))



// PATCH Method

 fetch('https://jsonplaceholder.typicode.com/todos/1', {
 		method: 'PATCH',
 		headers: {
 			'Content-Type': 'application/json'
 		},
 		body: JSON.stringify({
 			
 			status: "Complete",
 			dateCompleted: "07/09/21",
 			
 			
 		})

 })
.then((response) => response.json())
.then((json) => console.log(json))


// DELETE

 fetch('https://jsonplaceholder.typicode.com/todos/1', {
 		method: 'DELETE',


 })
.then((response) => response.json())
.then((json) => console.log(json))